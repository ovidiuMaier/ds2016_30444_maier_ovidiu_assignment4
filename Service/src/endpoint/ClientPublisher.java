package endpoint;

import javax.xml.ws.Endpoint;
import ws.ClientImpl;

//Endpoint publisher
public class ClientPublisher{

	public static void main(String[] args) {
	   Endpoint.publish("http://localhost:9999/ws/client", new ClientImpl());
    }

}
