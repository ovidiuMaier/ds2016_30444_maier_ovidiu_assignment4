package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Entry;
import model.Package;
import util.DbUtil;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.jws.WebService;

public class EntryDao {
	private Connection connection;

    public EntryDao() throws SQLException {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/package", "root", "");
    }
    
    public Entry[] getEntriesByName(String name) {
        Entry[] entries = new Entry[100];
        try {
            
            PreparedStatement preparedStatement = connection.
                    prepareStatement("select * from entries where name=?");
            preparedStatement.setString(1, name);
            ResultSet rs = preparedStatement.executeQuery();
            int k = 0;
            while (rs.next()) {
                Entry entry = new Entry();
                entry.setEntryid(rs.getInt("entryid"));
                entry.setName(rs.getString("name"));
                entry.setCity(rs.getString("city"));
                entry.setTime(rs.getString("time"));
                entries[k] = entry;
                k++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entries;
    }


}
