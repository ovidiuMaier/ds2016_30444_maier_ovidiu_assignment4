package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import model.Package;
import util.DbUtil;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import javax.jws.WebService;

public class PackageDao {

    private Connection connection;

    public PackageDao() throws SQLException {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/package", "root", "");
    }

//    public void addPackage(Package package1) {
//        try {
//            PreparedStatement preparedStatement = connection
//                    .prepareStatement("insert into package1s(firstname,lastname,dob,email) values (?, ?, ?, ? )");
//            // Parameters start with 1
//            preparedStatement.setString(1, package1.getFirstName());
//            preparedStatement.setString(2, package1.getLastName());
//            preparedStatement.setDate(3, new java.sql.Date(package1.getDob().getTime()));
//            preparedStatement.setString(4, package1.getEmail());
//            preparedStatement.executeUpdate();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }

//    public void deletePackage(int package1Id) {
//        try {
//            PreparedStatement preparedStatement = connection
//                    .prepareStatement("delete from package1s where package1id=?");
//            // Parameters start with 1
//            preparedStatement.setInt(1, package1Id);
//            preparedStatement.executeUpdate();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }

//    public void updatePackage(Package package1) {
//        try {
//            PreparedStatement preparedStatement = connection
//                    .prepareStatement("update package1s set firstname=?, lastname=?, dob=?, email=?" +
//                            "where package1id=?");
//            // Parameters start with 1
//            preparedStatement.setString(1, package1.getFirstName());
//            preparedStatement.setString(2, package1.getLastName());
//            preparedStatement.setDate(3, new java.sql.Date(package1.getDob().getTime()));
//            preparedStatement.setString(4, package1.getEmail());
//            preparedStatement.setInt(5, package1.getPackageid());
//            preparedStatement.executeUpdate();
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }

    public Package[] getAllPackages() {
        Package[] packages = new Package[100];
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("select * from packages");
            int k = 0;
            while (rs.next()) {
                Package package1 = new Package();
                package1.setPackageid(rs.getInt("packageid"));
                package1.setSender(rs.getString("sender"));
                package1.setReceiver(rs.getString("receiver"));
                package1.setName(rs.getString("name"));
                package1.setDescription(rs.getString("description"));
                package1.setSenderCity(rs.getString("senderCity"));
                package1.setDestCity(rs.getString("destCity"));
                package1.setTracking(rs.getBoolean("tracking"));
                packages[k] = package1;
                k++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return packages;
    }

//    public Package getPackageById(int package1Id) {
//        Package package1 = new Package();
//        try {
//            PreparedStatement preparedStatement = connection.
//                    prepareStatement("select * from package1s where package1id=?");
//            preparedStatement.setInt(1, package1Id);
//            ResultSet rs = preparedStatement.executeQuery();
//
//            if (rs.next()) {
//                package1.setPackageid(rs.getInt("package1id"));
//                package1.setFirstName(rs.getString("firstname"));
//                package1.setLastName(rs.getString("lastname"));
//                package1.setDob(rs.getDate("dob"));
//                package1.setEmail(rs.getString("email"));
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//
//        return package1;
//    }
    
  public Package getPackageByName(String name) {
  Package package1 = new Package();
  try {
      PreparedStatement preparedStatement = connection.
              prepareStatement("select * from packages where name=?");
      preparedStatement.setString(1, name);
      ResultSet rs = preparedStatement.executeQuery();

      if (rs.next()) {
    	  package1.setPackageid(rs.getInt("packageid"));
          package1.setSender(rs.getString("sender"));
          package1.setReceiver(rs.getString("receiver"));
          package1.setName(rs.getString("name"));
          package1.setDescription(rs.getString("description"));
          package1.setSenderCity(rs.getString("senderCity"));
          package1.setDestCity(rs.getString("destCity"));
          package1.setTracking(rs.getBoolean("tracking"));
      }
  } catch (SQLException e) {
      e.printStackTrace();
  }

  return package1;
}
}