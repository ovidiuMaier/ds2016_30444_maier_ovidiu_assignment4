package ws;

import java.sql.SQLException;
import java.util.List;

import javax.jws.WebService;

import model.*;
import model.Package;
import dao.*;

//Service Implementation
@WebService(endpointInterface = "ws.Client")
public class ClientImpl implements Client{

	@Override
	public User login(String email, String password){
		User user2 = new User();
		user2.setUserid(20000);
		user2.setEmail("1");
		user2.setPassword("1");
		user2.setAdmin(0);
		UserDao userDao = null;
		try {
			userDao = new UserDao();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		User user = userDao.getUserByEmail(email);
		int found = 0;
		if(user.getEmail()!= null && user.getPassword().equals(password)) found = 1;
		
		if (user.getEmail() != null && found == 1) {
			return user;
		} else {
			return user2;
		}		
	}
	
	@Override
	public void register(int userid, String email, String password){
		UserDao userDao = null;
		try {
			userDao = new UserDao();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		User user = new User();
		user.setUserid(userid);
		user.setEmail(email);
		user.setPassword(password);
		user.setAdmin(0);
		userDao.addUser(user);
	}
	
	
	@Override
	public Package[] getAllPackages(){
		PackageDao dao = null;
		try {
			dao = new PackageDao();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dao.getAllPackages();
	}
	
	@Override
	public Package searchPackage(String name){
		PackageDao dao = null;
		try {
			dao = new PackageDao();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dao.getPackageByName(name);
	}
	
	@Override
	public Entry[] getEntriesByName(String name){
		EntryDao dao = null;
		try {
			dao = new EntryDao();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dao.getEntriesByName(name);
	}

}