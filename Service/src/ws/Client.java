package ws;

import java.sql.SQLException;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import model.Package;
import model.Entry;
import model.User;

//Service Endpoint Interface
@WebService
@SOAPBinding(style = Style.RPC)
public interface Client{

	@WebMethod Entry[] getEntriesByName(String name);
	
	@WebMethod Package[] getAllPackages();
	
	@WebMethod Package searchPackage(String name);
	
	@WebMethod User login(String email, String password);
	
	@WebMethod void register(int userid, String email, String password);

}