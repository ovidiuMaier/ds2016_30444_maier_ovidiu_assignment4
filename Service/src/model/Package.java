package model;

public class Package {
	
	private int packageid;
	private String sender;
	private String receiver;
	private String name;
	private String description;
	private String senderCity;
	private String destCity;
	private boolean tracking;
	
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getReceiver() {
		return receiver;
	}
	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSenderCity() {
		return senderCity;
	}
	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}
	public String getDestCity() {
		return destCity;
	}
	public void setDestCity(String destCity) {
		this.destCity = destCity;
	}
	public boolean isTracking() {
		return tracking;
	}
	public void setTracking(boolean tracking) {
		this.tracking = tracking;
	}
	public int getPackageid() {
		return packageid;
	}
	public void setPackageid(int packageid) {
		this.packageid = packageid;
	}
	@Override
	public String toString() {
		return "Package [packageid=" + packageid + ", sender=" + sender + ", receiver=" + receiver + ", name=" + name
				+ ", description=" + description + ", senderCity=" + senderCity + ", destCity=" + destCity
				+ ", tracking=" + tracking + "]";
	}
	
}
