package model;

public class Entry {
	private int entryid;
	private String name;
	private String city;
	private String time;
	public int getEntryid() {
		return entryid;
	}
	public void setEntryid(int entryid) {
		this.entryid = entryid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	@Override
	public String toString() {
		return "Entry [entryid=" + entryid + ", name=" + name + ", city=" + city + ", time=" + time + "]";
	}
}
