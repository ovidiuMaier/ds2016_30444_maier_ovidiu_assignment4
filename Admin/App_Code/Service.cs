﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MySql.Data.MySqlClient;

[WebService(Namespace = "http://tempuri.org/",
Description = "A Simple Web Calculator Service",
Name = "CalculatorWebService")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]

public class Service : System.Web.Services.WebService
{
    public Service () {

        

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public int Add(int x, int y, String id, String sender, String receiver, String name, String description, String senderCity, String destCity, String tracking)
    {
        MySqlConnection myConnection = new MySqlConnection(
    "server=localhost; user id=root; password=''; database=package; pooling=false;");

        string query = "INSERT INTO packages (packageid, sender, receiver, name, description, senderCity, destCity, tracking) VALUES('" +
            id + "', '" + sender + "', '" + receiver + "', '" + name + "', '" + description + "', '" + senderCity + "', '" + destCity + "', '" + tracking + "');";  

    //open connection

        myConnection.Open();
        //create command and assign the query and connection from the constructor
        MySql.Data.MySqlClient.MySqlCommand myCommand = new MySql.Data.MySqlClient.MySqlCommand(query, myConnection);
        
        //Execute command
        myCommand.ExecuteNonQuery();
        return x + y;
    }
    [WebMethod]
    public int Subtract(int x, int y, String name)
    {
        MySqlConnection myConnection = new MySqlConnection(
   "server=localhost; user id=root; password=''; database=package; pooling=false;");

        string query = "DELETE FROM packages where name='" + name + "';";

        //open connection

        myConnection.Open();
        //create command and assign the query and connection from the constructor
        MySql.Data.MySqlClient.MySqlCommand myCommand = new MySql.Data.MySqlClient.MySqlCommand(query, myConnection);

        //Execute command
        myCommand.ExecuteNonQuery();
        return x - y;
    }
    [WebMethod]
    public int Multiply(int x, int y, String name)
    {
        MySqlConnection myConnection = new MySqlConnection(
   "server=localhost; user id=root; password=''; database=package; pooling=false;");

        string query = "update packages set tracking='1' where name='" + name + "';";

        //open connection

        myConnection.Open();
        //create command and assign the query and connection from the constructor
        MySql.Data.MySqlClient.MySqlCommand myCommand = new MySql.Data.MySqlClient.MySqlCommand(query, myConnection);

        //Execute command
        myCommand.ExecuteNonQuery();
        return x * y;
    }
    [WebMethod]
    public int Division(int x, int y, String id, String name, String city, String time)
    {

        MySqlConnection myConnection = new MySqlConnection(
   "server=localhost; user id=root; password=''; database=package; pooling=false;");

        string query = "INSERT INTO entries (entryid, name, city, time) VALUES('" +
            id + "', '" + name + "', '" + city + "', '" + time + "');";

        //open connection

        myConnection.Open();
        //create command and assign the query and connection from the constructor
        MySql.Data.MySqlClient.MySqlCommand myCommand = new MySql.Data.MySqlClient.MySqlCommand(query, myConnection);

        //Execute command
        myCommand.ExecuteNonQuery();
       
        return x / y;
    }
    
}