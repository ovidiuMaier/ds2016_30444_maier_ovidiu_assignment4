<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Show All Entries</title>
</head>
<body>
    <table border=1>
        <thead>
            <tr>
       
                <th>Name</th>
       
       
                <th>City</th>
                <th>Time</th>
           
            </tr>
        </thead>
        <tbody>
            <c:forEach var="i" begin="0" end="4">
                <tr>
                    
                    <td><c:out value="${entries[i].name}" /></td>
                    <td><c:out value="${entries[i].city}" /></td>
                    <td><c:out value="${entries[i].time}" /></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    
</body>
</html>