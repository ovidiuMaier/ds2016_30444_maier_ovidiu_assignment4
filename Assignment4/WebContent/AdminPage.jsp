<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin Page</title>
</head>
<body>
<a href="AdminController?action=add">Add Package</a>
<a href="AdminController?action=remove">Remove Package</a>
<a href="AdminController?action=track">Register For Tracking</a>
<a href="AdminController?action=addEntry">Update Status</a>

<form action="LogoutServlet" method="post">
	<input type="submit" value="Logout" >
</form>
</body>
</html>