<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Show All Packages</title>
</head>
<body>
    <table border=1>
        <thead>
            <tr>
                <th>Sender</th>
                <th>Receiver</th>
                <th>Name</th>
                <th>Description</th>
                <th>Sender City</th>
                <th>Destination City</th>
                <th>Tracking</th>
           
            </tr>
        </thead>
        <tbody>
            <c:forEach var="i" begin="0" end="1">
                <tr>
                    <td><c:out value="${packages[i].getSender()}" /></td>
                    <td><c:out value="${packages[i].getReceiver()}" /></td>
                    <td><c:out value="${packages[i].name}" /></td>
                    <td><c:out value="${packages[i].description}" /></td>
                    <td><c:out value="${packages[i].senderCity}" /></td>
                    <td><c:out value="${packages[i].destCity}" /></td>
                    <td><c:out value="${packages[i].tracking}" /></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    
</body>
</html>