package context;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Set;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.mysql.jdbc.AbandonedConnectionCleanupThread;
import com.mysql.jdbc.Driver;
import com.mysql.jdbc.log.LogFactory;
import com.sun.istack.internal.logging.Logger;

public class ContextFinalizer implements ServletContextListener {

//    private static final Logger LOGGER = LogFactory.getLogger(ContextFinalizer.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        Enumeration<java.sql.Driver> drivers = DriverManager.getDrivers();
        Driver d = null;
        while(drivers.hasMoreElements()) {
            try {
                d = (Driver) drivers.nextElement();
                DriverManager.deregisterDriver(d);
//                LOGGER.warn(String.format("Driver %s deregistered", d));
            } catch (SQLException ex) {
//                LOGGER.warn(String.format("Error deregistering driver %s", d), ex);
            }
        }
        try {
            AbandonedConnectionCleanupThread.shutdown();
        } catch (InterruptedException e) {
//            logger.warn("SEVERE problem cleaning up: " + e.getMessage());
            e.printStackTrace();
        }
    }

}