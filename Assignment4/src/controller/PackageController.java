package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import ws.*;



	public class PackageController extends HttpServlet {
	    private static final long serialVersionUID = 1L;
	    private static String LIST_PACKAGES = "/listPackages.jsp";
	 
	    public PackageController() {
	        super();
	        
	    }

	    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	        
	    	Client client = null;
			ClientImplServiceLocator clientServiceLocator = new ClientImplServiceLocator();
			try {
				client = clientServiceLocator.getClientImplPort();
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	    	String forward="";
	        String action = request.getParameter("action");
	        System.out.println(request.getParameter("action"));
	        if (action.equalsIgnoreCase("listPackages")){
	          forward = LIST_PACKAGES;
	          _package[] packages = new _package[100];
	          packages = client.getAllPackages();
	          System.out.println(packages[0].isTracking());
	         request.setAttribute("packages", client.getAllPackages());
	        }
	        
	        else if(action.equalsIgnoreCase("search"))
	        {
	        	 forward = "searchPackage.jsp";
	        }
	        	        
	        RequestDispatcher view = request.getRequestDispatcher(forward);
	        view.forward(request, response);
	    }
	    
	    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        
	        Client client = null;
	        
			ClientImplServiceLocator clientServiceLocator = new ClientImplServiceLocator();
			try {
				client = clientServiceLocator.getClientImplPort();
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			_package[] packages = new _package[100];
	          packages[0] = client.searchPackage(request.getParameter("name"));
//	          System.out.println(package1.getSender());
	          
	          request.setAttribute("packages", packages);
	          String forward = "packageResults.jsp";
	        RequestDispatcher view = request.getRequestDispatcher(forward);
	        view.forward(request, response);
	    }

}
