package controller;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import ws.*;



public class EntryController extends HttpServlet{
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EntryController() {
	        super();
	        
	    }

	    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	        
	    	
	    	String forward="";
	        String action = request.getParameter("action");
	        System.out.println(request.getParameter("action"));
	        if (action.equalsIgnoreCase("status")){
	          forward = "status.jsp";
	        }
	        
//	        else if(action.equalsIgnoreCase("search"))
//	        {
//	        	 forward = "searchPackage.jsp";
//	        }
//	        	        
	        RequestDispatcher view = request.getRequestDispatcher(forward);
	        view.forward(request, response);
	    }
	    
	    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        
	        Client client = null;
	        
			ClientImplServiceLocator clientServiceLocator = new ClientImplServiceLocator();
			try {
				client = clientServiceLocator.getClientImplPort();
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Entry[] entries = new Entry[100];
	          entries = client.getEntriesByName(request.getParameter("name"));
//	          System.out.println(package1.getSender());
	          
	          request.setAttribute("entries", entries);
	          String forward = "statusResults.jsp";
	        RequestDispatcher view = request.getRequestDispatcher(forward);
	        view.forward(request, response);
	    }

}
