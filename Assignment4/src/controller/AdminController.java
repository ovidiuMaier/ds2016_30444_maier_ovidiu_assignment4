package controller;

import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import ws.*;

import proxy.*;

public class AdminController extends HttpServlet{
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AdminController() {
	        super();
	        
	    }

	    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	        
	    	
	    	String forward="";
	        String action = request.getParameter("action");
	        System.out.println(request.getParameter("action"));
	        if (action.equalsIgnoreCase("add")){
	          forward = "addPackage.jsp";
	        }
	        
	        else if(action.equalsIgnoreCase("remove"))
	        {
	        	 forward = "removePackage.jsp";
	        }
	        else if(action.equalsIgnoreCase("track"))
	        {
	        	 forward = "track.jsp";
	        }
	        else if(action.equalsIgnoreCase("addEntry"))
	        {
	        	 forward = "addEntry.jsp";
	        }
	        
	        
	        	        
	        RequestDispatcher view = request.getRequestDispatcher(forward);
	        view.forward(request, response);
	    }
	    
	    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	        
	         
	    	CalculatorWebService service = new CalculatorWebService ();
			CalculatorWebServiceSoap soap = service.getPort(CalculatorWebServiceSoap.class);
			 
			String forward="";
			String action = request.getParameter("action");
			if(action.equalsIgnoreCase("add")){
				String id = request.getParameter("packageid");
				String sender = request.getParameter("sender");
				String receiver = request.getParameter("receiver");
				String name = request.getParameter("name");
				String description = request.getParameter("description");
				String senderCity = request.getParameter("senderCity");
				String destCity = request.getParameter("destCity");
				String tracking = request.getParameter("tracking");
				soap.add(2, 3, id, sender, receiver, name, description, senderCity, destCity, tracking);
				
			}
			
			else if (action.equalsIgnoreCase("remove")){
		         soap.subtract(3, 2, request.getParameter("name"));
		        }
			
			else if (action.equalsIgnoreCase("regtrack")){
		         soap.multiply(3, 2, request.getParameter("name"));
		        }
			
			else if (action.equalsIgnoreCase("addEntry")){
				String id = request.getParameter("entryid");
				String name = request.getParameter("name");
				
				
				String city = request.getParameter("city");
				String time = request.getParameter("time");
				 
				
				soap.division(6, 2, id, name, city, time);
		        }
	          
	        forward = "AdminPage.jsp";
	        RequestDispatcher view = request.getRequestDispatcher(forward);
	        view.forward(request, response);
	    }

}
