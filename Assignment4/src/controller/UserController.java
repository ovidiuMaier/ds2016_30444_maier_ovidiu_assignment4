package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import ws.*;



public class UserController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static String INSERT_OR_EDIT = "/user.jsp";
    private static String LIST_USER = "/listUser.jsp";
    private static String LIST_PACKAGES = "/listPackages.jsp";
    private static String ADD_USER = "/addUser.jsp";
    
    public UserController() {
        super();
        
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
    	Client client = null;
        
		ClientImplServiceLocator clientServiceLocator = new ClientImplServiceLocator();
		try {
			client = clientServiceLocator.getClientImplPort();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.setProperty("http.proxyHost", "");
		System.setProperty("proxyPort", "8080");
    	
//		CalculatorWebService service = new CalculatorWebService ();
//		CalculatorWebServiceSoap soap = service.getPort(CalculatorWebServiceSoap.class);
//				int x = 5,y = 10;
//				System.out.println(soap.add(x,y));
    	
    	String forward="";
        String action = request.getParameter("action");
        
        if (action.equalsIgnoreCase("listPackages")){
        	
        	System.out.println("dfgfdgsfdggggggggggggggg");
          forward = LIST_PACKAGES;
          _package[] packages = new _package[100];
          packages = client.getAllPackages();
          System.out.println(packages[0].isTracking());
         request.setAttribute("packages", client.getAllPackages());
        }
        
        else if (action.equalsIgnoreCase("register")){
        	forward = ADD_USER;
        }
        
        
        
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
        
    }

//        if (action.equalsIgnoreCase("delete")){
//            int userId = Integer.parseInt(request.getParameter("userId"));
//            dao.deleteUser(userId);
//            forward = LIST_USER;
//            request.setAttribute("users", dao.getAllUsers());    
//        } else if (action.equalsIgnoreCase("edit")){
//            forward = INSERT_OR_EDIT;
//            int userId = Integer.parseInt(request.getParameter("userId"));
//            User user = dao.getUserById(userId);
//            request.setAttribute("user", user);
//        } else if (action.equalsIgnoreCase("listUser")){
//            forward = LIST_USER;
//            request.setAttribute("users", dao.getAllUsers());
//        } else {
//            forward = INSERT_OR_EDIT;
//        }
//
//        RequestDispatcher view = request.getRequestDispatcher(forward);
//        view.forward(request, response);
//    }
//
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        Client client = null;
        
		ClientImplServiceLocator clientServiceLocator = new ClientImplServiceLocator();
		try {
			client = clientServiceLocator.getClientImplPort();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		client.register(Integer.parseInt(request.getParameter("userid")),request.getParameter("email"), request.getParameter("pwd"));
        
        RequestDispatcher view = request.getRequestDispatcher("login.html");
        view.forward(request, response);
    }
}