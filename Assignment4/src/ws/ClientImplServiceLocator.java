/**
 * ClientImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public class ClientImplServiceLocator extends org.apache.axis.client.Service implements ws.ClientImplService {

    public ClientImplServiceLocator() {
    }


    public ClientImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ClientImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ClientImplPort
    private java.lang.String ClientImplPort_address = "http://localhost:9999/ws/client";

    public java.lang.String getClientImplPortAddress() {
        return ClientImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ClientImplPortWSDDServiceName = "ClientImplPort";

    public java.lang.String getClientImplPortWSDDServiceName() {
        return ClientImplPortWSDDServiceName;
    }

    public void setClientImplPortWSDDServiceName(java.lang.String name) {
        ClientImplPortWSDDServiceName = name;
    }

    public ws.Client getClientImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ClientImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getClientImplPort(endpoint);
    }

    public ws.Client getClientImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ws.ClientImplPortBindingStub _stub = new ws.ClientImplPortBindingStub(portAddress, this);
            _stub.setPortName(getClientImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setClientImplPortEndpointAddress(java.lang.String address) {
        ClientImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ws.Client.class.isAssignableFrom(serviceEndpointInterface)) {
                ws.ClientImplPortBindingStub _stub = new ws.ClientImplPortBindingStub(new java.net.URL(ClientImplPort_address), this);
                _stub.setPortName(getClientImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ClientImplPort".equals(inputPortName)) {
            return getClientImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws/", "ClientImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws/", "ClientImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ClientImplPort".equals(portName)) {
            setClientImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
