package ws;

public class ClientProxy implements ws.Client {
  private String _endpoint = null;
  private ws.Client client = null;
  
  public ClientProxy() {
    _initClientProxy();
  }
  
  public ClientProxy(String endpoint) {
    _endpoint = endpoint;
    _initClientProxy();
  }
  
  private void _initClientProxy() {
    try {
      client = (new ws.ClientImplServiceLocator()).getClientImplPort();
      if (client != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)client)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)client)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (client != null)
      ((javax.xml.rpc.Stub)client)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public ws.Client getClient() {
    if (client == null)
      _initClientProxy();
    return client;
  }
  
  public void register(int arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (client == null)
      _initClientProxy();
    client.register(arg0, arg1, arg2);
  }
  
  public ws.User login(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (client == null)
      _initClientProxy();
    return client.login(arg0, arg1);
  }
  
  public ws._package[] getAllPackages() throws java.rmi.RemoteException{
    if (client == null)
      _initClientProxy();
    return client.getAllPackages();
  }
  
  public ws._package searchPackage(java.lang.String arg0) throws java.rmi.RemoteException{
    if (client == null)
      _initClientProxy();
    return client.searchPackage(arg0);
  }
  
  public ws.Entry[] getEntriesByName(java.lang.String arg0) throws java.rmi.RemoteException{
    if (client == null)
      _initClientProxy();
    return client.getEntriesByName(arg0);
  }
  
  
}