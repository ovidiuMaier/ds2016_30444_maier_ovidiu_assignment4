/**
 * ClientImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public interface ClientImplService extends javax.xml.rpc.Service {
    public java.lang.String getClientImplPortAddress();

    public ws.Client getClientImplPort() throws javax.xml.rpc.ServiceException;

    public ws.Client getClientImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
