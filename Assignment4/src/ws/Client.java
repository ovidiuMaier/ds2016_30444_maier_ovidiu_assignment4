/**
 * Client.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public interface Client extends java.rmi.Remote {
    public void register(int arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public ws.User login(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public ws._package[] getAllPackages() throws java.rmi.RemoteException;
    public ws._package searchPackage(java.lang.String arg0) throws java.rmi.RemoteException;
    public ws.Entry[] getEntriesByName(java.lang.String arg0) throws java.rmi.RemoteException;
}
